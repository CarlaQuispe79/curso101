<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                <svg width="393" height="54" xmlns="http://www.w3.org/2000/svg">
                    <g>
                    <title>Layer 1</title>
                    <text style="cursor: move;" fill="#C97A86" stroke-width="0" x="84.33161" y="41.26198" id="svg_2" font-size="30" font-family="'Emilys Candy'" text-anchor="start" xml:space="preserve" stroke="#000" font-style="normal" font-weight="bold">CURSO LARAVEL 101</text>
                    <path id="svg_1" d="m33.48809,24.80061c0.27365,0.19383 0.85277,-0.17328 1.13412,-0.21863c-0.31667,-0.44058 0.8761,-2.58698 0.46444,-3.36647c-0.12667,-1.76231 -3.33288,-5.74035 -3.73667,-6.20196c0.15403,-0.30957 1.88019,-0.0215 1.82866,0.02291c-0.72033,0.62101 2.43076,4.18502 2.79467,6.24684c0.67055,-0.03972 1.55134,-0.30859 1.94223,0.13556c0.36503,-0.42688 3.18156,-3.68856 3.33554,-6.50698c0.02207,-0.40389 1.55073,-0.14828 1.26317,0.0979c-0.46094,1.75477 -3.71143,6.68158 -3.96537,6.86096c0.29554,1.53635 0.17589,1.64166 2.0636,2.83628c0.84955,-2.13497 2.65969,-3.94724 4.31761,-5.75098c1.87206,-2.02524 3.12844,-3.96541 5.19062,-6.0737c2.71469,-2.67814 6.18166,-5.12183 9.35699,-6.58059c2.20894,-1.01479 5.90999,-1.88475 6.47735,-1.94168c1.05108,-0.10921 2.00159,-0.08982 3.24745,0.10829c1.83831,0.2923 3.63328,0.45554 5.56332,1.42514c1.8148,1.24441 1.18745,2.80402 0.84907,4.26489c-2.7951,3.29885 -5.65789,7.24967 -6.55951,11.29156c-0.30314,1.49663 -0.84819,4.07562 -2.13456,5.69561c-2.06806,2.49187 -4.59906,2.39972 -6.9771,2.73751c1.44899,0.44402 3.02057,1.89383 4.43049,3.35071c2.19941,2.29151 3.27111,4.64148 2.70314,7.19984c-0.12092,0.53661 -2.21801,4.60672 -3.24373,5.88383c-3.89317,4.86296 -9.19341,8.16094 -13.46529,7.62569c-1.53837,-0.19274 -5.7817,-3.64957 -6.54461,-4.6696c-1.89389,-2.53214 -3.44951,-5.58638 -3.51345,-8.25294c-0.23542,-0.25174 -0.52334,-0.27034 -0.39639,-0.60713c-0.2359,0.77121 -0.98501,3.28274 -2.09403,4.8705c-1.60332,2.29545 -3.07098,-1.1359 -3.66505,-3.91858c-0.20365,-0.9539 -0.05419,-1.11904 -0.29171,-1.68943c-1.12315,0.45608 -0.53177,1.77852 -0.97255,2.8468c-0.37369,0.93028 -2.04724,3.77172 -2.50939,4.41949c-1.47731,2.07063 -4.79451,5.20398 -7.20024,5.95584c-2.2328,0.69782 -4.1252,0.59992 -5.91969,-0.3851c-2.31458,-1.27049 -5.5765,-4.22116 -7.58325,-6.62036c-2.54043,-2.96865 -4.76082,-7.42891 -2.64753,-10.57067c1.34885,-1.96028 3.88783,-4.88162 6.3689,-5.59947c-1.75139,-0.22956 -5.62633,-1.46491 -7.06739,-4.89618c-0.46254,-1.08789 -0.63412,-2.77935 -0.75177,-3.98956c-0.19142,-1.99802 -1.04323,-4.29358 -1.75033,-6.26093c-0.56843,-1.79864 -1.5731,-4.10129 -2.62453,-6.06911c-1.1019,-2.06502 -2.00528,-5.47961 -0.08765,-7.16104c0.66986,-0.58744 2.74403,-1.2726 3.17751,-1.32146c1.51526,-0.17317 3.57967,0.62321 5.11216,1.10098c1.00401,0.31562 5.40935,2.00008 6.62765,2.92757c8.03165,6.11445 13.74275,13.73081 17.45309,20.74785z" stroke-width="0" stroke="#000" fill="#C97A86"/>
                    </g>

                 </svg>
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6">
                            <div class="flex items-center">
                            <svg width="226" height="65" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                <text style="cursor: move;" fill="#C97A86" stroke-width="0" x="71.88235" y="44.08398" id="svg_2" font-size="24" font-family="'Emilys Candy'" text-anchor="start" xml:space="preserve" stroke="#000" font-style="normal" font-weight="bold">TEMA NRO. 1</text>
                                <path id="svg_7" d="m21.4086,59.94361c3.41548,-3.44512 6.31412,-7.54845 7.53845,-12.2073c-3.66134,2.68882 -7.01934,7.01363 -12.12672,6.84978c-3.85576,0.21376 -8.44884,0.21738 -11.16298,-2.89762c-6.06533,-4.7113 -7.69465,-14.12766 -2.71407,-20.12345c3.40831,-4.32919 9.35998,-6.16489 14.82189,-5.51556c3.44143,-1.41051 -0.67311,-4.98206 -1.08689,-7.21108c-2.56135,-8.0855 3.47274,-17.78391 12.34539,-18.71534c6.36397,-0.88253 12.58527,3.09696 15.69926,8.26148c1.69223,3.61371 0.81052,7.71502 0.4157,11.464c-1.32952,1.884 -4.73458,6.44189 0.12456,6.08288c4.80429,-0.07344 9.87769,1.28897 13.04568,4.97935c4.63376,4.97019 5.05881,13.18347 0.39677,18.26209c-2.46608,3.09335 -6.16665,5.56915 -10.35503,5.43131c-4.09945,0.39976 -8.53853,-0.45931 -11.44551,-3.47654c-1.23983,-0.83921 -4.27782,-4.00991 -4.27678,-3.31456c3.03656,6.1107 6.47024,12.45658 12.54473,16.24322c2.17037,1.6857 -3.3058,0.52676 -4.63149,0.87033c-8.03212,0.00686 -16.06421,0.01385 -24.09634,0.02074c1.65444,-1.66793 3.30898,-3.33579 4.96335,-5.00372l0,0z" stroke-width="0" stroke="#000" fill="#C97A86"/>
                                </g>

                                </svg>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel has wonderful, thorough documentation covering every aspect of the framework. Whether you are new to the framework or have previous experience with Laravel, we recommend reading all of the documentation from beginning to end.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                            <div class="flex items-center">
                            <svg width="214" height="58" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                <text style="cursor: move;" fill="#C97A86" stroke-width="0" x="57.29021" y="39.32793" id="svg_2" font-size="24" font-family="'Emilys Candy'" text-anchor="start" xml:space="preserve" stroke="#000" font-style="normal" font-weight="bold">TEMA NRO. 2</text>
                                <path id="svg_9" d="m0,29.68142c5.33118,-10.99091 14.943,-20.37166 23.12002,-29.68142c9.51222,5.97441 17.73024,19.11802 24.87997,28.27226c-6.37708,10.5885 -15.55677,20.23173 -23.82646,29.72774c-8.91487,-8.68796 -16.53854,-18.61413 -24.17354,-28.31858l0,0z" stroke-width="0" stroke="#000" fill="#C97A86"/>
                                </g>

                            </svg>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laracasts offers thousands of video tutorials on Laravel, PHP, and JavaScript development. Check them out, see for yourself, and massively level up your development skills in the process.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                            <div class="flex items-center">
                                <svg width="211" height="44" xmlns="http://www.w3.org/2000/svg">
                                    <g>
                                    <title>Layer 1</title>
                                    <text style="cursor: move;" fill="#C97A86" stroke-width="0" x="53.79275" y="29.45196" id="svg_2" font-size="24" font-family="'Emilys Candy'" text-anchor="start" xml:space="preserve" stroke="#000" font-style="normal" font-weight="bold">TEMA NRO. 3</text>
                                    <path id="svg_11" d="m17.74243,38.8706c-13.04159,-10.25509 -17.70448,-16.74227 -17.74227,-24.68376c-0.03446,-7.23964 5.72163,-14.22696 11.68681,-14.18666c2.97874,0.02016 9.36834,2.68475 11.63092,4.85038c1.14002,1.09117 1.67999,0.98357 4.21812,-0.84056c6.90773,-4.9645 13.65319,-5.0687 18.02185,-0.27838c6.98205,7.65594 5.71089,16.79854 -3.65415,26.28188c-4.97899,5.04189 -15.85202,13.9865 -17.00193,13.9865c-0.3499,0 -3.57161,-2.30824 -7.15936,-5.12941l0,0z" stroke-width="0" stroke="#000" fill="#C97A86"/>
                                    </g>

                                </svg>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel News is a community driven portal and newsletter aggregating all of the latest and most important news in the Laravel ecosystem, including new package releases and tutorials.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                            <div class="flex items-center">
                            <svg width="209" height="53" xmlns="http://www.w3.org/2000/svg">
                            <g>
                            <text font-weight="bold" font-style="normal" stroke="#000" xml:space="preserve" text-anchor="start" font-family="'Emilys Candy'" font-size="24" id="svg_2" y="34.67598" x="53.30411" stroke-width="0" fill="#C97A86">TEMA NRO. 4</text>
                            <path id="svg_1" d="m14.15712,50.9878c3.00355,-3.15979 5.19987,-7.16115 7.04019,-11.21551c1.12421,-1.61707 0.05986,-3.03175 -1.47138,-1.66051c-3.93527,2.86438 -9.29207,3.84666 -13.55281,1.23341c-4.81661,-2.71521 -6.98864,-9.17168 -5.89579,-14.79747c0.69209,-5.70124 4.49842,-10.17011 8.63119,-13.38022c4.7474,-3.70225 9.87399,-6.88047 14.21284,-11.16751c1.81166,0.03959 3.17509,3.13879 5.12483,3.95891c5.17708,4.20372 11.31239,7.29799 15.38919,12.91913c2.05967,3.20299 3.23356,7.14707 3.36462,11.04585c-0.62754,5.28704 -3.51938,10.86289 -8.50638,12.36854c-4.31111,1.42202 -8.74659,-0.38139 -12.50343,-2.67572c-0.45888,-0.10736 -2.14201,-1.55294 -1.47494,-0.42951c2.24596,5.02458 4.70677,10.01388 8.27066,14.09356c0.20948,0.51547 1.85232,1.73844 0.978,1.71901c-7.3177,0 -14.63537,0 -21.95307,0c0.78208,-0.67065 1.56418,-1.34132 2.34629,-2.01197l0,0z" stroke-width="0" stroke="#000" fill="#C97A86"/>
                            </g>

                            </svg>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel's robust library of first-party tools and libraries, such as <a href="https://forge.laravel.com" class="underline">Forge</a>, <a href="https://vapor.laravel.com" class="underline">Vapor</a>, <a href="https://nova.laravel.com" class="underline">Nova</a>, and <a href="https://envoyer.io" class="underline">Envoyer</a> help you take your projects to the next level. Pair them with powerful open source libraries like <a href="https://laravel.com/docs/billing" class="underline">Cashier</a>, <a href="https://laravel.com/docs/dusk" class="underline">Dusk</a>, <a href="https://laravel.com/docs/broadcasting" class="underline">Echo</a>, <a href="https://laravel.com/docs/horizon" class="underline">Horizon</a>, <a href="https://laravel.com/docs/sanctum" class="underline">Sanctum</a>, <a href="https://laravel.com/docs/telescope" class="underline">Telescope</a>, and more.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
                    <div class="text-center text-sm text-gray-500 sm:text-left">
                        <div class="flex items-center">
                            <svg fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" class="-mt-px w-5 h-5 text-gray-400">
                                <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                            </svg>

                            <a href="https://laravel.bigcartel.com" class="ml-1 underline">
                                Shop
                            </a>

                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="ml-4 -mt-px w-5 h-5 text-gray-400">
                                <path d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path>
                            </svg>

                            <a href="https://github.com/sponsors/taylorotwell" class="ml-1 underline">
                                Sponsor
                            </a>
                        </div>
                    </div>

                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
